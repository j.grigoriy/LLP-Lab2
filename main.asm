
extern read_word
extern print_string
extern print_error
extern print_newline
extern string_length
extern find_word

global _start

section .text:

%include "colon.inc"


section .data:
error: db "Key is not found", 0
delimiter: db " - ", 0
%include "words.inc"

section .text:
_start:
    call read_word
    mov rdi, pointer
    call find_word
    test rax, rax
    jz .not_found
    add rax, 8
    mov rdi, rax
    push rax
    call string_length
    inc rax
    push rax
    call print_string
    mov rdi, delimiter
    call print_string
    pop rax
    mov rbx, rax
    pop rax
    add rax, rbx
    mov rdi, rax
    call print_string
    jmp .exit
.not_found:
    mov rdi, error
    call print_error
.exit:
    call print_newline
    mov rax, 60
    mov rdi, 0
    syscall
