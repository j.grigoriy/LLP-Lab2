
extern string_equals
global find_word

section .text:

; rax - word
; rdi - pointer
; str_eq - rdi & rsi, res - rax
find_word:
    mov rsi, rax
    xor rax, rax
.loop:
    test rdi, rdi
    jz .not_found
    push rdi
    add rdi, 8
    call string_equals
    pop rdi
    test rax, rax
    jnz .found
    mov rdi, [rdi]
    jmp .loop
.found:
    mov rax, rdi
    ret
.not_found:
    mov rax, 0
    ret

