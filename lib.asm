section .text

global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy
global print_error

; rdi - pointer to the string
string_length:
    xor rax, rax
    push rcx
    xor rcx, rcx
.string_length_loop:
    mov cl, [rdi+rax]
    inc rax
    cmp cl, 0x00		; test cl, cl
    jne .string_length_loop
    dec rax
    pop rcx
    ret

; rdi - pointer to the string
print_string:
    xor rax, rax
    push rcx
    push rsi
    call string_length
    mov rcx, rax
    mov rax, 1
    mov rsi, rdi
    mov rdi, 1
    mov rdx, rcx
    syscall
    pop rsi
    pop rcx
    ret

print_error:
    push rsi
    call string_length
    mov rdx, rax
    mov rax, 1
    mov rsi, rdi
    mov rdi, 2
    syscall
    pop rsi
    ret

; rdi - pointer to char
print_char:
    push rdi
    mov rax, 1
    mov rsi, rsp
    mov rdi, 1
    mov rdx, 1
    syscall
    pop rdi
    ret


print_newline:
    push rdi
    mov rdi, 0x0A
    call print_char
    pop rdi
    ret
; rdi - argument
print_uint:
    push r12
    xor r12, r12
    push r9
    mov r9, 0x0A
    mov rax, rdi
    xor r8, r8
    push 0x0
.loop:
    xor rdx, rdx
    div r9
    dec rsp
    add rdx, '0'
    mov byte [rsp], dl
    inc r8
    cmp rax, 0
    jne .loop

.print_uint:
    mov rdi, rsp
    call print_string
add rsp, r8
pop r8
pop r9
pop r12
ret

; rdi - arg
print_int:
    mov rax, rdi
    test rax, rax
    jns .pos_int
    mov rdi, '-'
    push rax
    call print_char
    pop rax
    not rax
    add rax, 1
    .pos_int:
    mov rdi, rax
    call print_uint
    ret

; rdi - pointer to 1 str
; rsi - pointer to 2 str
string_equals:
    push r9
    push r10
    push r11
    call string_length
    mov r9, rax
    push rdi
    mov rdi, rsi
    call string_length
    mov r10, rax
    pop rdi
    cmp r9, r10
    jne .no
    xor r9, r9
    xor r10, r10
    xor r11, r11
.equals_loop:
    mov r10b, [rdi+r9]
    mov r11b, [rsi+r9]
    cmp r10, r11
    jne .no
    cmp r10, 0
    je .yes
    inc r9
    jmp .equals_loop
    
.no:
    mov rax, 0
    jmp .end_equals

.yes:
    mov rax, 1
    jmp .end_equals

.end_equals:
    pop r11
    pop r10
    pop r9
    ret


read_char:
    push 0
    mov rax, 0
    mov rdi, 0
    mov rsi, rsp
    mov rdx, 1
    syscall
    pop rax
    ret 

section .data
word_buffer times 256 db 0

section .text


; прочитать с потока ввода следующее слово, пропустив перед пробельные символы
; возвращает: в rax - слово, в rdx — длина слова
read_word:
    xor r8, r8
    mov r8, word_buffer
    .skip:
    call read_char
    test rax, rax
    jz .finish
    cmp rax, 32
    jle .skip
    .read_next:
    mov [r8], rax
    inc r8
    call read_char
    cmp rax, 31
    jg .read_next
    .finish:
    mov rdi, word_buffer
    call string_length
    mov rdx, rax
    mov rax, word_buffer
    ret

; rdi points to a string
; returns rax: number, rdx : length
parse_uint:
    xor rax, rax
    push r9
    push r10
    push r11
    xor r8, r8
    xor r10, r10
    mov r11, 0x0A
    xor r9, r9
    .skip_uint:
    mov r10b, [rdi + r9]
    inc r9
    cmp r10, ' '
    je .skip_uint
    cmp r10, '0'
    jb .end
    cmp r10, '9'
    ja .end
    
    xor rdx, rdx
    
    .parse_uint_loop:
    sub r10, '0'
    mul r11
    add rax, r10
    mov r10b, [rdi+r9]
    inc r9
    inc r8
    cmp r10, '0'
    jb .end
    cmp r10, '9'
    ja .end
    jmp .parse_uint_loop

    .end:
    dec r9
    mov rdx, r8
    pop r11
    pop r10
    pop r9
    ret

; rdi points to a string
; returns rax: number, rdx : length
parse_int:
    xor rax, rax
    push r9
    push r10
    xor r10, r10
    xor r9, r9
    
    .skip_int:
    mov r10b, [rdi+r9]
    inc r9
    cmp r10, ' '
    je .skip_int
    cmp r10, '-'
    je .neg_int
    cmp r10, '+'
    je .plus_int
    cmp r10, '0'
    jb .end_int
    cmp r10, '9'
    ja .end_int
    jmp .pos_int
    
    .neg_int:
    add rdi, r9
    call parse_uint
    not rax
    add rax, 1
    add rdx, 1
    jmp .end_int

    .plus_int:
    inc r9
    
    .pos_int:
    dec r9
    call parse_uint

    .end_int:
    pop r10
    pop r9
    ret 


; rdi points to string
; rsi points to address
; rdx - string_length
string_copy:
    xor rax, rax
    push rbx
    push rcx
    xor rcx, rcx
    call string_length
    cmp rax, rdx
    jae .too_long
    mov rax, rsi
.string_copy_loop:
    mov bl, byte [rdi]
    mov byte [rsi], bl
    inc rdi
    inc rsi
    inc rcx
    cmp rcx, rdx
    jne .string_copy_loop
    jmp .end_copy

.too_long:
    xor rax, rax

.end_copy:
    pop rcx
    pop rbx    
    ret
